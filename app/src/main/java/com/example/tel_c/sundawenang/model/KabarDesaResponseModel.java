package com.example.tel_c.sundawenang.model;

import java.util.List;

/**
 * Created by TEL-C on 5/17/17.
 */

public class KabarDesaResponseModel {

    String kode, pesan;
    List<KabarDesaModel> result;

    public List<KabarDesaModel> getResult() {
        return result;
    }

    public void setResult(List<KabarDesaModel> result) {
        this.result = result;
    }

    public String getKode() {
        return kode;
    }

    public void setKode(String kode) {
        this.kode = kode;
    }

    public String getPesan() {
        return pesan;
    }

    public void setPesan(String pesan) {
        this.pesan = pesan;
    }
}
