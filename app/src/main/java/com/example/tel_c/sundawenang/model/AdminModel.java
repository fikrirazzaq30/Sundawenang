package com.example.tel_c.sundawenang.model;

import com.google.gson.annotations.SerializedName;

/**
 * Created by TEL-C on 5/15/17.
 */

public class AdminModel {

    @SerializedName("admin_id")
    String admin_id;
    @SerializedName("admin_usnm")
    String admin_usnm;
    @SerializedName("admin_pswd")
    String admin_pswd;
    @SerializedName("admin_nama")
    String admin_nama;
    @SerializedName("rw_id")
    String rw_id;
    @SerializedName("rt_id")
    String rt_id;

    public String getAdmin_id() {
        return admin_id;
    }

    public void setAdmin_id(String admin_id) {
        this.admin_id = admin_id;
    }

    public String getAdmin_usnm() {
        return admin_usnm;
    }

    public void setAdmin_usnm(String admin_usnm) {
        this.admin_usnm = admin_usnm;
    }

    public String getAdmin_pswd() {
        return admin_pswd;
    }

    public void setAdmin_pswd(String admin_pswd) {
        this.admin_pswd = admin_pswd;
    }

    public String getAdmin_nama() {
        return admin_nama;
    }

    public void setAdmin_nama(String admin_nama) {
        this.admin_nama = admin_nama;
    }

    public String getRw_id() {
        return rw_id;
    }

    public void setRw_id(String rw_id) {
        this.rw_id = rw_id;
    }

    public String getRt_id() {
        return rt_id;
    }

    public void setRt_id(String rt_id) {
        this.rt_id = rt_id;
    }
}
