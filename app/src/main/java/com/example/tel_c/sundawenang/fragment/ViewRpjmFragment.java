package com.example.tel_c.sundawenang.fragment;


import android.app.ProgressDialog;
import android.content.Intent;
import android.os.Bundle;
import android.support.design.widget.FloatingActionButton;
import android.support.design.widget.Snackbar;
import android.support.v4.app.Fragment;
import android.support.v4.widget.SwipeRefreshLayout;
import android.support.v7.widget.LinearLayoutManager;
import android.support.v7.widget.RecyclerView;
import android.util.Log;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;

import com.example.tel_c.sundawenang.R;
import com.example.tel_c.sundawenang.activity.LoginActivity;
import com.example.tel_c.sundawenang.activity.admin.InputRpjmActivity;
import com.example.tel_c.sundawenang.adapter.AdapterRpjm;
import com.example.tel_c.sundawenang.api.ApiRequest;
import com.example.tel_c.sundawenang.api.RetroServer;
import com.example.tel_c.sundawenang.model.RpjmResponseModel;
import com.example.tel_c.sundawenang.model.RpjmModel;
import com.example.tel_c.sundawenang.model.RtModel;
import com.example.tel_c.sundawenang.model.RtResponseModel;

import java.util.List;

import retrofit2.Call;
import retrofit2.Callback;
import retrofit2.Response;

/**
 * A simple {@link Fragment} subclass.
 */
public class ViewRpjmFragment extends Fragment {

    private RecyclerView mRecycler;
    private RecyclerView.Adapter mAdapter;
    private RecyclerView.LayoutManager mManager;
    private ProgressDialog progressDialog;
    private List<RpjmModel> rpjmModelList;
    private List<RtModel> rtModelList;
    SwipeRefreshLayout swipeRefreshLayout;
    String rpjm_id, rpjm_bobot, admin_id, rw_id, rt_id;
    String rt_id_, rt_nama, rt_kk, rt_kkbyrpjk, rw_id_;

    public static ViewRpjmFragment newInstance() {
        ViewRpjmFragment fragment = new ViewRpjmFragment();
        return fragment;
    }

    public ViewRpjmFragment() {
        // Required empty public constructor
    }

    @Override
    public View onCreateView(LayoutInflater inflater, ViewGroup container,
                             Bundle savedInstanceState) {
        // Inflate the layout for this fragment
        View v = inflater.inflate(R.layout.fragment_view_rpjm, container, false);

        Intent intent = getActivity().getIntent();
        rw_id = intent.getStringExtra("rw_id");
        rt_id = intent.getStringExtra("rt_id");
        admin_id = intent.getStringExtra("admin_id");
        rpjm_id = intent.getStringExtra("rpjm_id");

        loadDataRt();

        FloatingActionButton fab = (FloatingActionButton) v.findViewById(R.id.fab_rpjm);
        fab.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                Intent intent = new Intent(getActivity(), InputRpjmActivity.class);
                intent.putExtra("admin_id", admin_id);
                intent.putExtra("rw_id", rw_id);
                intent.putExtra("rt_id", rt_id);



                startActivity(intent);
            }
        });

        mRecycler = (RecyclerView) v.findViewById(R.id.recyclerTemp);
        loadDataRpjm();

        swipeRefreshLayout = (SwipeRefreshLayout) v.findViewById(R.id.swp_refresh_rpjm);

        swipeRefreshLayout.setOnRefreshListener(new SwipeRefreshLayout.OnRefreshListener() {
            @Override
            public void onRefresh() {

                loadDataRpjm();
                swipeRefreshLayout.setRefreshing(false);
            }
        });

        return v;
    }

    private void loadDataRpjm() {
        mManager = new LinearLayoutManager(getActivity(), LinearLayoutManager.VERTICAL, false);
        mRecycler.setLayoutManager(mManager);

        progressDialog = new ProgressDialog(getActivity());
        progressDialog.setMessage("Mengambil data...");
        progressDialog.setCancelable(false);
        progressDialog.show();

        ApiRequest api = RetroServer.getClient().create(ApiRequest.class);
        Call<RpjmResponseModel> getRpjm = api.getRpjm();
        getRpjm.enqueue(new Callback<RpjmResponseModel>() {
            @Override
            public void onResponse(Call<RpjmResponseModel> call, Response<RpjmResponseModel> response) {
                progressDialog.hide();
                Log.d("Retrofit", "Response: " + response.body().getKode());
                rpjmModelList = response.body().getResult();

                mAdapter = new AdapterRpjm(getActivity(), rpjmModelList);
                mRecycler.setAdapter(mAdapter);
                mAdapter.notifyDataSetChanged();
            }

            @Override
            public void onFailure(Call<RpjmResponseModel> call, Throwable t) {
                progressDialog.hide();
                Log.d("Retrofit", "Failure: Response Gagal");
            }
        });
    }

    private void loadDataRt() {
        ApiRequest api = RetroServer.getClient().create(ApiRequest.class);
        Call<RtResponseModel> getRt = api.getRt();
        getRt.enqueue(new Callback<RtResponseModel>() {
            @Override
            public void onResponse(Call<RtResponseModel> call, Response<RtResponseModel> response) {
                rtModelList = response.body().getResult();
            }

            @Override
            public void onFailure(Call<RtResponseModel> call, Throwable t) {

            }
        });
    }
}