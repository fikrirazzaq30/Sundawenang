package com.example.tel_c.sundawenang.adapter;

import android.content.Context;
import android.support.v7.widget.RecyclerView;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.TextView;

import com.example.tel_c.sundawenang.R;
import com.example.tel_c.sundawenang.model.KabarDesaModel;

import java.util.List;

/**
 * Created by TEL-C on 5/16/17.
 */

public class AdapterKabarDesa extends RecyclerView.Adapter<AdapterKabarDesa.HolderData> {

    private List<KabarDesaModel> kabarDesaModelList;
    private Context mContext;

    public AdapterKabarDesa(List<KabarDesaModel> kabarDesaModelList, Context mContext) {
        this.kabarDesaModelList = kabarDesaModelList;
        this.mContext = mContext;
    }

    @Override
    public HolderData onCreateViewHolder(ViewGroup parent, int viewType) {
        View layout = LayoutInflater.from(parent.getContext()).inflate(R.layout.list_item_kabardesa, parent, false);
        HolderData holderData = new HolderData(layout);
        return holderData;
    }

    @Override
    public void onBindViewHolder(HolderData holder, int position) {
        KabarDesaModel kabarDesaModel = kabarDesaModelList.get(position);
        holder.txJudul.setText(kabarDesaModel.getKb_judul());
        holder.txTgl.setText(kabarDesaModel.getKb_tgl());
    }

    @Override
    public int getItemCount() {
        return kabarDesaModelList.size();
    }

    class HolderData extends RecyclerView.ViewHolder {

        TextView txJudul, txTgl;
        KabarDesaModel kabarDesaModel;

        public HolderData (View v) {
            super(v);

            txJudul = (TextView) v.findViewById(R.id.tx_judul);
            txTgl = (TextView) v.findViewById(R.id.tx_tgl);
        }
    }
}
