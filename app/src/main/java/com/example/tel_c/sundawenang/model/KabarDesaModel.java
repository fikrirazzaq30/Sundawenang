package com.example.tel_c.sundawenang.model;

/**
 * Created by TEL-C on 5/15/17.
 */

public class KabarDesaModel {

    String kb_id;
    String kb_judul;
    String kb_isi;
    String kb_tgl;
    String admin_id;

    public String getKb_id() {
        return kb_id;
    }

    public void setKb_id(String kb_id) {
        this.kb_id = kb_id;
    }

    public String getKb_judul() {
        return kb_judul;
    }

    public void setKb_judul(String kb_judul) {
        this.kb_judul = kb_judul;
    }

    public String getKb_isi() {
        return kb_isi;
    }

    public void setKb_isi(String kb_isi) {
        this.kb_isi = kb_isi;
    }

    public String getKb_tgl() {
        return kb_tgl;
    }

    public void setKb_tgl(String kb_tgl) {
        this.kb_tgl = kb_tgl;
    }

    public String getAdmin_id() {
        return admin_id;
    }

    public void setAdmin_id(String admin_id) {
        this.admin_id = admin_id;
    }
}
