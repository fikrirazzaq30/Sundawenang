package com.example.tel_c.sundawenang.adapter;

import android.content.Context;
import android.content.DialogInterface;
import android.content.Intent;
import android.support.v7.app.AlertDialog;
import android.support.v7.widget.PopupMenu;
import android.support.v7.widget.RecyclerView;
import android.util.Log;
import android.view.LayoutInflater;
import android.view.MenuItem;
import android.view.View;
import android.view.ViewGroup;
import android.widget.ImageView;
import android.widget.TextView;
import android.widget.Toast;

import com.example.tel_c.sundawenang.R;
import com.example.tel_c.sundawenang.activity.admin.InputRpjmActivity;
import com.example.tel_c.sundawenang.api.ApiRequest;
import com.example.tel_c.sundawenang.api.RetroServer;
import com.example.tel_c.sundawenang.model.KabarDesaModel;
import com.example.tel_c.sundawenang.model.RpjmResponseModel;

import java.util.List;

import retrofit2.Call;
import retrofit2.Callback;
import retrofit2.Response;

/**
 * Created by TEL-C on 5/18/17.
 */

public class AdapterKabarDesaAdmin extends RecyclerView.Adapter<AdapterKabarDesaAdmin.HolderData> {

    private List<KabarDesaModel> kabarDesaModelList;
    private Context mContext;

    public AdapterKabarDesaAdmin(List<KabarDesaModel> kabarDesaModelList, Context mContext) {
        this.kabarDesaModelList = kabarDesaModelList;
        this.mContext = mContext;
    }

    @Override
    public HolderData onCreateViewHolder(ViewGroup parent, int viewType) {
        View layout = LayoutInflater.from(parent.getContext()).inflate(R.layout.list_item_kabar_desa_admin, parent, false);
        HolderData holderData = new HolderData(layout);
        return holderData;
    }

    @Override
    public void onBindViewHolder(final HolderData holder, int position) {
        KabarDesaModel kabarDesaModel = kabarDesaModelList.get(position);
        holder.txJudul.setText(kabarDesaModel.getKb_judul());
        holder.txTgl.setText(kabarDesaModel.getKb_tgl());

        holder.btnViewOption.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {

                PopupMenu popupMenu = new PopupMenu(mContext, holder.btnViewOption);
                popupMenu.inflate(R.menu.menu_ubahhapus);
                popupMenu.setOnMenuItemClickListener(new PopupMenu.OnMenuItemClickListener() {
                    @Override
                    public boolean onMenuItemClick(MenuItem item) {
                        switch (item.getItemId()) {
                            case R.id.menu_ubah:
                                break;
                            case R.id.menu_hapus:
                                break;
                        }
                        return false;
                    }
                });

                popupMenu.show();
            }
        });
    }

    @Override
    public int getItemCount() {
        return kabarDesaModelList.size();
    }


    class HolderData extends RecyclerView.ViewHolder {

        TextView txJudul, txTgl;
        KabarDesaModel kabarDesaModel;
        ImageView btnViewOption;


        public HolderData (View v) {
            super(v);
            btnViewOption = (ImageView) v.findViewById(R.id.img_dots_admin);
            txJudul = (TextView) v.findViewById(R.id.tx_judul_admin);
            txTgl = (TextView) v.findViewById(R.id.tx_tgl_admin);


        }
    }
}
