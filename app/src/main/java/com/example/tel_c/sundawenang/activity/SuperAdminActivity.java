package com.example.tel_c.sundawenang.activity;

import android.content.Intent;
import android.os.Bundle;
import android.support.annotation.NonNull;
import android.support.design.widget.BottomNavigationView;
import android.support.v4.app.Fragment;
import android.support.v4.app.FragmentTransaction;
import android.support.v7.app.AppCompatActivity;
import android.view.MenuItem;
import android.widget.TextView;
import android.widget.Toast;

import com.example.tel_c.sundawenang.R;
import com.example.tel_c.sundawenang.activity.admin.InputRpjmActivity;
import com.example.tel_c.sundawenang.fragment.ProfilFragment;
import com.example.tel_c.sundawenang.fragment.ViewKabarDesaAdminFragment;
import com.example.tel_c.sundawenang.fragment.ViewRpjmFragment;

public class SuperAdminActivity extends AppCompatActivity {

    private TextView mTextMessage;
    String rw_id, rt_id;
    Intent dataAdmin;

    private BottomNavigationView.OnNavigationItemSelectedListener mOnNavigationItemSelectedListener
            = new BottomNavigationView.OnNavigationItemSelectedListener() {

        @Override
        public boolean onNavigationItemSelected(@NonNull MenuItem item) {
            Fragment selectedFragment = null;
            switch (item.getItemId()) {
                case R.id.navigation_kabardesa_su:
                    //Kabar Desa
                    selectedFragment = ViewKabarDesaAdminFragment.newInstance();
                    break;
                case R.id.navigation_rpjm_su:
                    //Rpjm
                    selectedFragment = ViewRpjmFragment.newInstance();
                    break;
                case R.id.navigation_regis_su:
                    //Regis User
                    Intent intentRegis = new Intent(SuperAdminActivity.this, RegisAdminActivity.class);
                    startActivity(intentRegis);
                    return true;
                case R.id.navigation_profil_su:
                    //Profil
                    selectedFragment = ProfilFragment.newInstance();
                    break;
            }
            FragmentTransaction transaction = getSupportFragmentManager().beginTransaction();
            transaction.replace(R.id.content_su, selectedFragment);
            transaction.commit();
            return true;
        }
    };

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_super_admin);

        BottomNavigationView navigation = (BottomNavigationView) findViewById(R.id.navigation_su);
        navigation.setOnNavigationItemSelectedListener(mOnNavigationItemSelectedListener);

        FragmentTransaction transaction = getSupportFragmentManager().beginTransaction();
        transaction.replace(R.id.content_su, ViewKabarDesaAdminFragment.newInstance());
        transaction.commit();

        dataAdmin = getIntent();
        Toast.makeText(SuperAdminActivity.this, "Selamat datang, "
                + dataAdmin.getExtras().get("admin_nama") + "!", Toast.LENGTH_LONG).show();


        Intent kirimRwRt = new Intent(SuperAdminActivity.this, InputRpjmActivity.class);
        kirimRwRt.putExtra("rw_id", rw_id);
        kirimRwRt.putExtra("rt_id", rt_id);
    }

}
