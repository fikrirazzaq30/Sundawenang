package com.example.tel_c.sundawenang.activity;

import android.app.ProgressDialog;
import android.support.v7.app.AppCompatActivity;
import android.os.Bundle;
import android.util.Log;
import android.view.View;
import android.widget.Button;
import android.widget.EditText;
import android.widget.Toast;

import com.example.tel_c.sundawenang.R;
import com.example.tel_c.sundawenang.activity.admin.InputRpjmActivity;
import com.example.tel_c.sundawenang.api.ApiRequest;
import com.example.tel_c.sundawenang.api.RetroServer;
import com.example.tel_c.sundawenang.model.AdminResponseModel;

import retrofit2.Call;
import retrofit2.Callback;
import retrofit2.Response;

public class RegisAdminActivity extends AppCompatActivity {

    EditText edtNamaLengkap, edtUsname, edtPswd, edtRw, edtRt;
    Button btnDaftar;
    ProgressDialog progressDialog;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_regis_admin);

        getSupportActionBar().setDisplayHomeAsUpEnabled(true);
        getSupportActionBar().setDisplayShowHomeEnabled(true);

        edtNamaLengkap = (EditText) findViewById(R.id.edt_namalengkap);
        edtUsname = (EditText) findViewById(R.id.edt_usname);
        edtPswd = (EditText) findViewById(R.id.edt_pswd);
        edtRw = (EditText) findViewById(R.id.edt_rw);
        edtRt = (EditText) findViewById(R.id.edt_rt);

        btnDaftar = (Button) findViewById(R.id.btn_daftar);

        progressDialog = new ProgressDialog(this);

        btnDaftar.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                progressDialog.setMessage("Menyimpan data...");
                progressDialog.setCancelable(false);
                progressDialog.show();

                String sNama = edtNamaLengkap.getText().toString();
                String sUsnm = edtUsname.getText().toString();
                String sPswd = edtPswd.getText().toString();
                String sRw = edtRw.getText().toString();
                String sRt = edtRt.getText().toString();

                ApiRequest api = RetroServer.getClient().create(ApiRequest.class);

                Call<AdminResponseModel> regisAdmin = api.regisAdmin(sNama, sUsnm, sPswd, sRw, sRt);
                regisAdmin.enqueue(new Callback<AdminResponseModel>() {
                    @Override
                    public void onResponse(Call<AdminResponseModel> call, Response<AdminResponseModel> response) {
                        progressDialog.hide();
                        Log.d("Retrofit", "response: " + response.body().toString());
                        String kode = response.body().getKode();

                        if (kode.equals("1")) {
                            Toast.makeText(RegisAdminActivity.this, "Data berhasil disimpan", Toast.LENGTH_LONG).show();
                            edtNamaLengkap.setText("");
                            edtUsname.setText("");
                            edtPswd.setText("");
                            edtRw.setText("");
                            edtRt.setText("");
                        } else {
                            Toast.makeText(RegisAdminActivity.this, "Data gagal disimpan", Toast.LENGTH_LONG).show();
                        }
                    }

                    @Override
                    public void onFailure(Call<AdminResponseModel> call, Throwable t) {
                        progressDialog.hide();
                        Log.d("Retrofit", "Failure: " + "Gagal Mengirim Request" );
                    }
                });
            }
        });
    }

    @Override
    public boolean onSupportNavigateUp() {
        onBackPressed();
        return true;
    }
}