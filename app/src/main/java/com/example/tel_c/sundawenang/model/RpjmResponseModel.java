package com.example.tel_c.sundawenang.model;

import java.util.List;

/**
 * Created by TEL-C on 5/15/17.
 */

public class RpjmResponseModel {

    String kode, pesan;
    List<RpjmModel> result;

    public List<RpjmModel> getResult() {
        return result;
    }

    public void setResult(List<RpjmModel> result) {
        this.result = result;
    }

    public String getKode() {
        return kode;
    }

    public void setKode(String kode) {
        this.kode = kode;
    }

    public String getPesan() {
        return pesan;
    }

    public void setPesan(String pesan) {
        this.pesan = pesan;
    }
}
