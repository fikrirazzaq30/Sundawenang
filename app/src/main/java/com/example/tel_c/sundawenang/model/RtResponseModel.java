package com.example.tel_c.sundawenang.model;

import java.util.List;

/**
 * Created by TEL-C on 5/18/17.
 */

public class RtResponseModel {

    String kode, pesan;
    List<RtModel> result;

    public List<RtModel> getResult() {
        return result;
    }

    public void setResult(List<RtModel> result) {
        this.result = result;
    }

    public String getKode() {
        return kode;
    }

    public void setKode(String kode) {
        this.kode = kode;
    }

    public String getPesan() {
        return pesan;
    }

    public void setPesan(String pesan) {
        this.pesan = pesan;
    }
}
