package com.example.tel_c.sundawenang.fragment;


import android.app.ProgressDialog;
import android.os.Bundle;
import android.support.v4.app.Fragment;
import android.support.v7.widget.LinearLayoutManager;
import android.support.v7.widget.RecyclerView;
import android.util.Log;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;

import com.example.tel_c.sundawenang.R;
import com.example.tel_c.sundawenang.activity.admin.ViewRpjmActivity;
import com.example.tel_c.sundawenang.adapter.AdapterKabarDesa;
import com.example.tel_c.sundawenang.adapter.AdapterRpjm;
import com.example.tel_c.sundawenang.api.ApiRequest;
import com.example.tel_c.sundawenang.api.RetroServer;
import com.example.tel_c.sundawenang.model.KabarDesaModel;
import com.example.tel_c.sundawenang.model.KabarDesaResponseModel;
import com.example.tel_c.sundawenang.model.RpjmModel;

import java.util.List;

import retrofit2.Call;
import retrofit2.Callback;
import retrofit2.Response;

/**
 * A simple {@link Fragment} subclass.
 */
public class ViewKabarDesaFragment extends Fragment {

    private RecyclerView mRecycler;
    private RecyclerView.Adapter mAdapter;
    private RecyclerView.LayoutManager mManager;

    private ProgressDialog progressDialog;
    private List<KabarDesaModel> kabarDesaModels;

    public ViewKabarDesaFragment() {
        // Required empty public constructor
    }


    @Override
    public View onCreateView(LayoutInflater inflater, ViewGroup container,
                             Bundle savedInstanceState) {
        // Inflate the layout for this fragment
        View v = inflater.inflate(R.layout.fragment_view_kabar_desa, container, false);

        mRecycler = (RecyclerView) v.findViewById(R.id.recyclerKabarDesa);
        mManager = new LinearLayoutManager(getActivity(), LinearLayoutManager.VERTICAL, false);
        mRecycler.setLayoutManager(mManager);

        progressDialog = new ProgressDialog(getActivity());
        progressDialog.setMessage("Mengambil data...");
        progressDialog.setCancelable(false);
        progressDialog.show();

        ApiRequest api = RetroServer.getClient().create(ApiRequest.class);
        Call<KabarDesaResponseModel> getKabarDesa = api.getKabarDesa();
        getKabarDesa.enqueue(new Callback<KabarDesaResponseModel>() {
            @Override
            public void onResponse(Call<KabarDesaResponseModel> call, Response<KabarDesaResponseModel> response) {
                progressDialog.hide();
                Log.d("Retrofit", "Response: " + response.body().getKode());
                kabarDesaModels = response.body().getResult();

                mAdapter = new AdapterKabarDesa(kabarDesaModels, getActivity());
                mRecycler.setAdapter(mAdapter);
                mAdapter.notifyDataSetChanged();
            }

            @Override
            public void onFailure(Call<KabarDesaResponseModel> call, Throwable t) {
                progressDialog.hide();
                Log.d("Retrofit", "Failure: Response Gagal");
            }
        });

        return v;
    }

}
