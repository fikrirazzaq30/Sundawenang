package com.example.tel_c.sundawenang.activity.admin;

import android.app.ProgressDialog;
import android.support.v7.app.AppCompatActivity;
import android.os.Bundle;
import android.support.v7.widget.LinearLayoutManager;
import android.support.v7.widget.RecyclerView;
import android.util.Log;

import com.example.tel_c.sundawenang.R;
import com.example.tel_c.sundawenang.adapter.AdapterRpjm;
import com.example.tel_c.sundawenang.api.ApiRequest;
import com.example.tel_c.sundawenang.api.RetroServer;
import com.example.tel_c.sundawenang.model.RpjmResponseModel;
import com.example.tel_c.sundawenang.model.RpjmModel;

import java.util.List;

import retrofit2.Call;
import retrofit2.Callback;
import retrofit2.Response;

public class ViewRpjmActivity extends AppCompatActivity {

    private RecyclerView mRecycler;
    private RecyclerView.Adapter mAdapter;
    private RecyclerView.LayoutManager mManager;
    private ProgressDialog progressDialog;
    private List<RpjmModel> rpjmModelList;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_view_rpjm);

        mRecycler = (RecyclerView) findViewById(R.id.recyclerTemp);
        mManager = new LinearLayoutManager(this, LinearLayoutManager.VERTICAL, false);
        mRecycler.setLayoutManager(mManager);

        progressDialog = new ProgressDialog(this);
        progressDialog.setMessage("Mengambil data...");
        progressDialog.setCancelable(false);
        progressDialog.show();

        ApiRequest api = RetroServer.getClient().create(ApiRequest.class);
        Call<RpjmResponseModel> getRpjm = api.getRpjm();
        getRpjm.enqueue(new Callback<RpjmResponseModel>() {
            @Override
            public void onResponse(Call<RpjmResponseModel> call, Response<RpjmResponseModel> response) {
                progressDialog.hide();
                Log.d("Retrofit", "Response: " + response.body().getKode());
                rpjmModelList = response.body().getResult();

                mAdapter = new AdapterRpjm(ViewRpjmActivity.this, rpjmModelList);
                mRecycler.setAdapter(mAdapter);
                mAdapter.notifyDataSetChanged();
            }

            @Override
            public void onFailure(Call<RpjmResponseModel> call, Throwable t) {
                progressDialog.hide();
                Log.d("Retrofit", "Failure: Response Gagal");
            }
        });
    }
}
