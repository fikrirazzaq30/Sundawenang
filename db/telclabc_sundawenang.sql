-- phpMyAdmin SQL Dump
-- version 4.6.6
-- https://www.phpmyadmin.net/
--
-- Host: localhost:3306
-- Generation Time: 12 Jun 2017 pada 14.09
-- Versi Server: 5.5.54-cll
-- PHP Version: 5.6.30

SET SQL_MODE = "NO_AUTO_VALUE_ON_ZERO";
SET time_zone = "+00:00";


/*!40101 SET @OLD_CHARACTER_SET_CLIENT=@@CHARACTER_SET_CLIENT */;
/*!40101 SET @OLD_CHARACTER_SET_RESULTS=@@CHARACTER_SET_RESULTS */;
/*!40101 SET @OLD_COLLATION_CONNECTION=@@COLLATION_CONNECTION */;
/*!40101 SET NAMES utf8mb4 */;

--
-- Database: `telclabc_sundawenang`
--

-- --------------------------------------------------------

--
-- Struktur dari tabel `t_admin`
--

CREATE TABLE `t_admin` (
  `admin_id` int(6) UNSIGNED NOT NULL,
  `admin_usnm` varchar(16) NOT NULL,
  `admin_pswd` varchar(16) NOT NULL,
  `admin_nama` varchar(50) NOT NULL,
  `rw_id` int(6) DEFAULT NULL,
  `rt_id` int(6) DEFAULT NULL
) ENGINE=MyISAM DEFAULT CHARSET=latin1;

--
-- Dumping data untuk tabel `t_admin`
--

INSERT INTO `t_admin` (`admin_id`, `admin_usnm`, `admin_pswd`, `admin_nama`, `rw_id`, `rt_id`) VALUES
(1, 'superadmin', 'superadmin', 'Super Admin', NULL, NULL),
(2, 'juvetic', 'juvetic', 'Juvetic', NULL, NULL),
(3, 'razzaq', 'razzaq', 'Razzaq', NULL, NULL),
(6, 'qhansa', 'qhansa', 'Qhansa Di\'Ayu Putri', NULL, NULL),
(12, 'adminrt1rw1', 'adminrt1rw1', 'Admin RW1/RT1', 1, 1),
(8, 'laela', 'laela', 'Laela Citra Asih', NULL, NULL),
(13, 'adminrw2', 'adminrw2', 'Admin RW2', 2, NULL);

-- --------------------------------------------------------

--
-- Struktur dari tabel `t_kabardesa`
--

CREATE TABLE `t_kabardesa` (
  `kb_id` int(6) UNSIGNED NOT NULL,
  `kb_judul` varchar(100) NOT NULL,
  `kb_isi` varchar(5000) NOT NULL,
  `kb_tgl` date NOT NULL,
  `admin_id` int(6) NOT NULL
) ENGINE=MyISAM DEFAULT CHARSET=latin1;

--
-- Dumping data untuk tabel `t_kabardesa`
--

INSERT INTO `t_kabardesa` (`kb_id`, `kb_judul`, `kb_isi`, `kb_tgl`, `admin_id`) VALUES
(1, 'Akuntabilitas Kementerian Desa Dipuji Kementerian PAN-RB\n', 'Rapor akuntabilitas Kementerian Desa, Pembangunan Daerah Tertinggal, dan Transmigrasi meningkat pesat dari 53,98 pada 2015 menjadi  60,95 di 2016. Hal tersebut diungkapkan Sekretaris Deputi Bidang Reformasi Birokrasi, Akuntabilitas Aparatur, dan Pengawasan, Kementerian Pendayagunaan Aparatur Negara dan Reformasi Birokrasi, Didid Noordiatmoko.\n\n\n\n\"Artinya, efektivitas dan efisiensi penggunaan anggaran Kementerian Desa, Pembangunan Daerah Tertinggal, dan Transmigrasi semakin membaik. Jika tahun sebelumnya kami tidak bisa me-list kembali kinerja ini menggunakan anggaran yang mana atau kalau uangnya dihabiskan sekian kinerjanya apa,\" ujarnya.\n\n\n\nDidin menuturkan, walaupun belum mencapai angka sempurna, arah kinerja sudah sejalan dengan Rencana Pembangunan Jangka Menengah (RPJM). \"Tentu lebih membaik meskipun beberapa perbaikan harus tetap dilakukan,\" ucapnya.\n\n\n\nSelain itu, evaluasi terkait dengan pelaksanaan reformasi birokrasi meningkat cukup signifikan, dari sebelumnya 50,60 pada 2015 menjadi 63,79 di 2016. Selain itu, menurutnya, persepsi positif masyarakat terhadap Kementerian Desa juga  meningkat.\n\n\n\n\"Sudah memperbaiki integritas. Jadi reformasi bukan semata-mata untuk menyusun berbagai  dokumen. Namun bagaimana perubahan berdampak positif terhadap peningkatan kesejahteraan. Arahnya ke sana, kinerja ini terkait dengan bagaimana kesejahteraan masyarakat menjadi lebih baik,\" tuturnya.\n\n\n\nDidid menjelaskan, tujuan pelaksanaan reformasi birokrasi oleh Kementerian PAN-RB berkaitan dengan tiga hal, yakni mewujudkan birokrasi yang bersih dan akuntabel, birokrasi yang efektif dan efisien, dan birokrasi yang mampu memberikan pelayanan publik yang semakin baik. \"Arahnya hanya tiga itu. Dengan demikian, apa pun yang dilakukan harus mengarah pada tiga hal itu,\" katanya.\n\n\n\nTerkait dengan hal tersebut, Menteri Desa, Pembangunan Daerah Tertinggal, dan Transmigrasi Eko Putro Sandjojo merasa bangga dan terharu atas kerja keras semua pihak, terutama kementerian. \"Walaupun belum sempurna, tapi bisa berubah. Tugas yang diemban kementerian adalah untuk membantu mengentaskan kemiskinan di Indonesia agar hidup masyarakat menjadi lebih baik,\" ujarnya.', '2017-05-17', 3),
(6, 'Demi Anak-anak Desa, Ibu Ini Modifikasi Motor Roda 3 Jadi Perpustakaan Keliling', 'Riuh suara anak-anak kecil yang sedang bercanda menyeruak di Mata Aksara Jalan Kaliurang Km 14, No 15 A, Tegalmanding, Umbulmartani, Ngemplak, Sleman.\r\n\r\nKeriuhan itu serentak berhenti, ketika Heni Wardatur Rohmah yang tidak lain adalah pendiri \"Mata Aksara\" mendekati motor roda tiga yang dimodifikasi dan membuka rolling door di bagian samping.\r\n\r\nBelum juga selesai membuka rolling door di bagian sisi belakang, anak-anak kecil yang tadinya berlarian, berdesak-desakan untuk masuk ke dalam ruangan kecil di belakang motor roda tiga tersebut.\r\n\r\nAnak yang rata-rata masih duduk di taman kanak-kanak dan sekolah dasar ini lantas melihat buku-buku yang tertata rapi di bagian dalam. Mereka satu per satu mengambil buku yang disukai lalu duduk dan membacanya.\r\n\r\nAnak yang belum bisa membaca pun antusias membuka lembar demi lembar buku meski hanya melihat ilustrasi gambar. Sepeda motor roda tiga itu adalah perpustakaan keliling yang digagas oleh Heni.', '2017-05-16', 1),
(3, 'Penerapan Undang-Undang Desa Masih Terhambat', 'Anggota Fraksi Partai Gerindra DPR RI Novita Wijaya menilai, Penerapan Undang-Undang Nomor 6 Tahun 2014 tentang Desa di Tanah Air masih terhambat sejumlah masalah.\r\n\r\nMenurut dia, paling tidak ada enam masalah yang menghambat penerapan Undang-Undang Desa. \"Pertama, fragmentasi penafsiran Undang-Undang Desa di tingkat elite yang berimplikasi pada proses implementasi dan pencapaian mandat yang tidak utuh, bahkan mengarah pada pembelokan terhadap mandat Undang-Undang Desa,\" ujar Novita dalam keterangannya kepada Republika.co.id, Rabu (17/5).\r\n\r\nBertempat di Aula Desa Kemiri, Novita menyosialisasikan Undang-Undang Desa kepada para kepala desa (kades) di Kecamatan Jeruklegi, Kesugihan, dan Wana Reja, Cilacap, Jawa Tengah, Rabu (16/5). Ia menambahkan, masalah kedua yang menghambat UU Desa adalah terjadi pragmatisme yang mengarah pada hilangnya kreativitas di tingkat pemerintahan desa dalam menggali sumber daya lokal di desa. \r\n\r\nMenurut dia, dana desa yang seharusnya dimanfaatkan untuk meningkatkan kemandirian dan kesejahteraan masyarakat Desa belum digunakan secara optimal. \"Ketiga, demokratisasi di desa masih menghadapi kendala praktik  administratif. Aparatur pemerintah daerah cenderung melakukan tindakan kepatuhan dari  pemerintah pusat  untuk mengendalikan pemerintah desa, termasuk Dana Desa,\" \r\n\r\nMasalah keempat, papar Novita, praktik pelaksanaan musyawarah desa cenderung patriarkis. Menurut dia, peran perempuan mengalami marjinalisasi ketika mereka menyampaikan usulan yang berkaitan dengan kepentingan tubuh, nalar, dan keberlangsungan hidupnya.\r\n\r\nKelima, lanjut dia, tata ruang kawasan pedesaan yang harus tunduk dengan tata daerah cenderung tidak sesuai dengan aspirasi desa. Pembangunan desa skala lokal terkendala dengan pola kebijakan tata ruang perdesaan yang berpola top-down. \"Keenam, soal penguasaan lahan. Penguasaan rakyat atas tanah dan sumber daya alam yang belum terintegrasi sebagai basis pembangunan desa,\" tuturnya.', '2017-05-17', 1),
(5, 'Desa Tamansari di Kaki Gunung Ijen Raih \"Desa Wisata Award\"\n', 'Desa Tamansari, Kecamatan Licin, Kabupaten Banyuwangi, berhasil menjadi desa wisata terbaik dalam kategori pemanfaatan jejaring bisnis dari Kementerian Desa, Pembangunan Desa Tertinggal dan Transmigrasi.\n\nPenghargaan tersebut diraih karena desa yang berada tepat di bawah kaki Gunung Ijen dinilai berhasil merintis pengembangan potensi wisata seperti homestay, kendaraan wisata, jasa guide serta beberapa usaha kecil menengah yang dikelola oleh Badan Usaha Milik Desa (BUMDesa) Ijen Lestari.\nKepada Kompas.com, Kamis (17/5/2107), Bambang Hadi S, kepala Bumdesa Ijen Lestari Desa Tamansari menjelaskan, saat ini mereka telah mengelola 53 penginapan milik masyarakat Desa Tamansari. Pihaknya juga membantu pemilik penginapan untuk promosi via online travel agent.\n\n\"Di Desa Tamansari banyak wisatawan yang menginap di homestay milik warga sebelum mereka naik Gunung Ijen. Dan dari satu kamar yang disewa untuk wisatawan, pemilik rumah akan membayar 10.000 rupiah kepada Bumdesa. Sedangkan untuk harga kamar rata-rata antara 150 ribu sampai 200 ribu rupiah per malam. Kami bantu jualkan kamar mereka lewat online dan media sosial,\" kata Hadi.\n\nUniknya, kerja sama antara Bumdesa dan pemilik homestay berdasarkan asas kepercayaan dan pembayaran bisa dilakukan kapan saja.\n\n\"Mau ada tamu langsung kasih uang Bumdesa atau seminggu sekali atau sebulan sekali kami tidak pernah menagih. Termasuk berapa kamar yang dipakai kami sudah saling percaya,\" kata Bambang sambil tersenyum.\n\nIa mejelaskan rata-rata pemilik homestay bekerja sebagai petani dan penambang sehingga pihak Bumdesa juga membantu mereka untuk memberikan pelatihan cara menata kamar di homestay dan pelayanan kepada wisatawan yang datang.\n\n\"Minimal kamar bersih, rapi, ada tempat tidur lemari dan meja kursi. Tidak harus mewah. Kamar untuk homestay menjadi satu dengan pemilik rumah,\" jelasnya.\n\nSelain mengelola homestay, Bumdesa Ijen Lestari juga mengembangkan sayap ke paket wisata di Desa Wisata Tamansari yang disingkat Dewi Tari. Bukan hanya berkunjung ke Gunung Ijen, wisatawan yang datang juga diajak untuk menikmati objek-objek wisata yang ada di Desa Tamansari seperi Kampung Bunga dan Kampung Susu serta kampung Penambang dan menikmati kopi produksi masyarakat desa.\n\nWisatawan juga diajak untuk menelusuri hutan pinus dan mempelajari kearifan lokal masyarakat Desa Tamansari. Ide mengelola paket wisata itu muncul, karena selama ini Desa Tamansari hanya dilewati oleh wisatawan saat berkunjung ke Gunung Ijen.\n\nDengan adanya paket wisata Dewi Tari, diharapkan wisatawan akan tinggal lebih lama di Desa Tamansari sehingga berdampak baik pada perekonomian masyarakat desa.\n\n\"Jika tinggal lebih lama, maka pemilik homestay akan untung, yang jualan makanan juga beruntung dan wisatawan juga akan beli produk masyarakat. Untuk paket wisata lengkap biasanya 3 hari 3 malam,\" jelasnya.', '2017-05-17', 7),
(7, 'Demi Anak-anak Desa, Ibu Ini Modifikasi Motor Roda 3 Jadi Perpustakaan Keliling', 'Riuh suara anak-anak kecil yang sedang bercanda menyeruak di Mata Aksara Jalan Kaliurang Km 14, No 15 A, Tegalmanding, Umbulmartani, Ngemplak, Sleman.\r\n\r\nKeriuhan itu serentak berhenti, ketika Heni Wardatur Rohmah yang tidak lain adalah pendiri \"Mata Aksara\" mendekati motor roda tiga yang dimodifikasi dan membuka rolling door di bagian samping.\r\n\r\nBelum juga selesai membuka rolling door di bagian sisi belakang, anak-anak kecil yang tadinya berlarian, berdesak-desakan untuk masuk ke dalam ruangan kecil di belakang motor roda tiga tersebut.\r\n\r\nAnak yang rata-rata masih duduk di taman kanak-kanak dan sekolah dasar ini lantas melihat buku-buku yang tertata rapi di bagian dalam. Mereka satu per satu mengambil buku yang disukai lalu duduk dan membacanya.\r\n\r\nAnak yang belum bisa membaca pun antusias membuka lembar demi lembar buku meski hanya melihat ilustrasi gambar. Sepeda motor roda tiga itu adalah perpustakaan keliling yang digagas oleh Heni.', '2017-05-16', 7);

-- --------------------------------------------------------

--
-- Struktur dari tabel `t_rpjm`
--

CREATE TABLE `t_rpjm` (
  `rpjm_id` int(6) UNSIGNED NOT NULL,
  `rpjm_nama` varchar(50) NOT NULL,
  `rpjm_ket` varchar(100) NOT NULL,
  `rpjm_jmlwarga` int(6) NOT NULL,
  `rpjm_biaya` float NOT NULL,
  `rpjm_lamapemb` int(6) NOT NULL,
  `rpjm_bobot` int(3) NOT NULL,
  `admin_id` int(6) NOT NULL
) ENGINE=MyISAM DEFAULT CHARSET=latin1;

--
-- Dumping data untuk tabel `t_rpjm`
--

INSERT INTO `t_rpjm` (`rpjm_id`, `rpjm_nama`, `rpjm_ket`, `rpjm_jmlwarga`, `rpjm_biaya`, `rpjm_lamapemb`, `rpjm_bobot`, `admin_id`) VALUES
(18, 'Perbaikan Jalan Cempaka', 'Akses Sarana Transportasi Desa', 6500, 300000000, 1, 3, 2),
(17, 'Jembatan Mawar', 'Akses Penyeberangan Sungai', 4000, 100000000, 2, 3, 2),
(16, 'TK Terpadu Al-Hidayah Gedung Pustaka', 'Menambah Wawasan Siswa', 120, 400000000, 2, 2, 2),
(15, 'PAUD Cempaka Ruang Kelas Belajar', 'Melancarkan Kegiatan Belajar Mengajar', 50, 150000000, 3, 2, 2),
(10, 'SDN 020 Ruang Kelas Belajar (2 Unit)', 'Melancarkan Kegiatan Belajar Mengajar', 720, 300000000, 3, 2, 7),
(11, 'SDN 025 Ruang Kelas Belajar (2 Unit)', 'Melancarkan Kegiatan Belajar Mengajar', 560, 300000000, 3, 2, 7),
(12, 'SMPN Satu Atap Ruang Kelas Belajar (3 Unit)', 'Melancarkan Kegiatan Belajar Mengajar', 1850, 400000000, 3, 3, 7),
(13, 'TK Terpadu Al-Hidayah Ruang Kelas Belajar', 'Melancarkan Kegiatan Belajar Mengajar', 95, 150000000, 3, 2, 7),
(14, 'SDN 020 Gedung Pustaka', 'Menambah Wawasan Siswa', 720, 200000000, 3, 2, 7),
(20, 'Pembangunan PLTN', 'PLTN dibangun atas perintah dari pemerintah pusat', 396, 1000000000, 9, 2, 9);

-- --------------------------------------------------------

--
-- Struktur dari tabel `t_rt`
--

CREATE TABLE `t_rt` (
  `rt_id` int(6) UNSIGNED NOT NULL,
  `rt_nama` varchar(10) NOT NULL,
  `rt_kk` int(10) NOT NULL,
  `rt_kkbyrpjk` int(10) DEFAULT NULL,
  `rw_id` int(6) DEFAULT NULL
) ENGINE=MyISAM DEFAULT CHARSET=latin1;

--
-- Dumping data untuk tabel `t_rt`
--

INSERT INTO `t_rt` (`rt_id`, `rt_nama`, `rt_kk`, `rt_kkbyrpjk`, `rw_id`) VALUES
(1, '1', 35, 10, 1),
(2, '2', 40, 25, 1),
(3, '3', 25, 5, 1),
(4, '4', 30, 30, 1),
(5, '1', 35, 30, 2),
(6, '2', 35, 30, 2),
(7, '3', 15, 12, 2),
(8, '4', 20, 4, 2),
(9, '1', 40, 15, 3),
(10, '2', 40, 20, 3),
(11, '3', 18, 8, 3),
(12, '4', 24, 21, 3);

-- --------------------------------------------------------

--
-- Struktur dari tabel `t_rw`
--

CREATE TABLE `t_rw` (
  `rw_id` int(6) UNSIGNED NOT NULL,
  `rw_nama` varchar(10) NOT NULL
) ENGINE=MyISAM DEFAULT CHARSET=latin1;

--
-- Dumping data untuk tabel `t_rw`
--

INSERT INTO `t_rw` (`rw_id`, `rw_nama`) VALUES
(1, '1'),
(2, '2'),
(3, '3');

--
-- Indexes for dumped tables
--

--
-- Indexes for table `t_admin`
--
ALTER TABLE `t_admin`
  ADD PRIMARY KEY (`admin_id`);

--
-- Indexes for table `t_kabardesa`
--
ALTER TABLE `t_kabardesa`
  ADD PRIMARY KEY (`kb_id`);

--
-- Indexes for table `t_rpjm`
--
ALTER TABLE `t_rpjm`
  ADD PRIMARY KEY (`rpjm_id`);

--
-- Indexes for table `t_rt`
--
ALTER TABLE `t_rt`
  ADD PRIMARY KEY (`rt_id`);

--
-- Indexes for table `t_rw`
--
ALTER TABLE `t_rw`
  ADD PRIMARY KEY (`rw_id`);

--
-- AUTO_INCREMENT for dumped tables
--

--
-- AUTO_INCREMENT for table `t_admin`
--
ALTER TABLE `t_admin`
  MODIFY `admin_id` int(6) UNSIGNED NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=14;
--
-- AUTO_INCREMENT for table `t_kabardesa`
--
ALTER TABLE `t_kabardesa`
  MODIFY `kb_id` int(6) UNSIGNED NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=8;
--
-- AUTO_INCREMENT for table `t_rpjm`
--
ALTER TABLE `t_rpjm`
  MODIFY `rpjm_id` int(6) UNSIGNED NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=23;
--
-- AUTO_INCREMENT for table `t_rt`
--
ALTER TABLE `t_rt`
  MODIFY `rt_id` int(6) UNSIGNED NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=13;
--
-- AUTO_INCREMENT for table `t_rw`
--
ALTER TABLE `t_rw`
  MODIFY `rw_id` int(6) UNSIGNED NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=6;
/*!40101 SET CHARACTER_SET_CLIENT=@OLD_CHARACTER_SET_CLIENT */;
/*!40101 SET CHARACTER_SET_RESULTS=@OLD_CHARACTER_SET_RESULTS */;
/*!40101 SET COLLATION_CONNECTION=@OLD_COLLATION_CONNECTION */;
